import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
import random
import string
import sys


# name-test_laravel
# email-laravel@yahoo.com
# pass-lary30
# conf-lary30

class Testlaravel(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome('D:\proiecte curs\chromedriver.exe')
        self.driver.get('https://laravel.dev-society.com/')

    def tearDown(self):
        self.driver.quit()

    def user_login(self, email, password):
        login_page = self.driver.find_element_by_xpath('//*[@id="login_button"]')
        login_page.click()
        time.sleep(1)
        self.assertIn('com/login', self.driver.current_url)

        email_box = self.driver.find_element_by_xpath('//*[@id="email"]')
        pass_box = self.driver.find_element_by_xpath('//*[@id="password"]')

        email_box.clear()
        pass_box.clear()

        email_box.send_keys(email)
        pass_box.send_keys(password)

        login_button = self.driver.find_element_by_xpath('//*[@id="login_user"]')
        login_button.click()
        time.sleep(2)
        # self.assertIn('/dashboard', self.driver.page_source)

    def user_register(self, name, email, password):

        register_button = self.driver.find_element_by_xpath('//*[@id="register_button"]')
        register_button.click()
        name_box = self.driver.find_element_by_xpath('//*[@id="name"]')
        name_box.clear()
        name_box.send_keys(name)

        email_box = self.driver.find_element_by_xpath('//*[@id="email"]')
        email_box.clear()
        email_box.send_keys(email)

        password_box = self.driver.find_element_by_xpath('//*[@id="password"]')
        password_box.clear()
        password_box.send_keys(password)

        confirm_password = self.driver.find_element_by_xpath('//*[@id="password-confirm"]')
        confirm_password.clear()
        confirm_password.send_keys(password)

        register_button = self.driver.find_element_by_xpath('//*[@id="register_account"]')
        register_button.click()

    def test_aboutPage(self):

        about_us = self.driver.find_element_by_xpath('//*[@id="about_page"]')
        about_us.click()
        self.assertIn('About us', self.driver.page_source )

    def RegisterUser(self):

        self.user_register('test_laravel', 'laravel1@yahoo.com', 'lary30')
        self.assertIn('/dashboard', self.driver.current_url)


    def RegisterNewUser(self):
        random_name = ''.join(random.choices(string.ascii_lowercase, k=10)) + '_' + str(
            random.randint(1, 20))
        random_email = ''.join(random.choices(string.ascii_lowercase, k=10)) + '@domain.com'

        self.user_register(random_name, random_email, 'ParolaSigura@1213')
        self.assertIn('/dashboard', self.driver.current_url)


    def InvalidRegisterEmptyPassword(self):
      self.user_register('test_laravel', 'laravel1@yahoo.com', '')
      self.assertIn('/register', self.driver.current_url)


    def InvalidRegisterWrongEmail(self):
        self.user_register('test_laravel', 'laravel1', 'gigigi')
        self.assertIn('/register', self.driver.current_url)

    def InvalidRegisterEmpyNameBox(self):
        self.user_register('', 'laravel1@yahoo.com', 'gigigi')
        self.assertIn('/register', self.driver.current_url)


    def InvalidLoginEmptyEmail(self):
        self.user_login('', 'lary30')
        self.assertIn('/login', self.driver.current_url)

    def InvalidLoginEmptyPassword(self):
        self.user_login('laravel1@yahoo.com', '')
        self.assertIn('/login', self.driver.current_url)

    def InvalidLoginWrongEmail(self):
        self.user_login('fddhkl@yahoo.com', 'lary30')
        self.assertIn('/login', self.driver.current_url)
        self.assertIn('These credentials do not match our records.', self.driver.page_source)

    def InvalidLoginWrongPassword(self):
        self.user_login('laravel1@yahoo.com', 'lkjhgf')
        self.assertIn('/login', self.driver.current_url)
        self.assertIn('These credentials do not match our records.', self.driver.page_source)

    def DeleteAcount(self):
        self.user_login('laravel1@yahoo.com', 'lary30')
        self.assertIn('Dashboard', self.driver.page_source)

        settings_button = self.driver.find_element_by_xpath('//*[@id="sidebar-settings"]/a')
        settings_button.click()
        self.assertIn('/settings', self.driver.page_source)
        time.sleep(2)

        delete_button = self.driver.find_element_by_xpath('//*[@id="delete_button"]')
        delete_button.click()
        time.sleep(2)
        self.assertIn('DEV Society', self.driver.page_source)
        self.assertIn('https://laravel.dev-society.com/', self.driver.current_url)

    def AddNewPost(self):
        self.user_login('laravel1@yahoo.com', 'lary30')
        self.assertIn('Dashboard', self.driver.page_source)

        new_post = self.driver.find_element_by_xpath('//*[@id="sidebar-posts"]/a')
        new_post.click()
        time.sleep(1)
        self.assertIn('/post', self.driver.current_url)


        addnew_post = self.driver.find_element_by_xpath('//*[@id="create_post"]')
        addnew_post.click()
        self.assertIn('/posts/create', self.driver.current_url)

        post_title = self.driver.find_element_by_xpath('/html/body/main/div/div/main/form/input[2]')
        post_title.clear()
        post_title.send_keys('Testare')

        write_post = self.driver.find_element_by_xpath('//*[@id="text_editor"]')
        write_post.clear()
        write_post.send_keys('Testare-Automata')

        submit_button =self.driver.find_element_by_xpath('//*[@id="submit_post"]')
        submit_button.click()
        self.assertIn('posts', self.driver.current_url)

        Testlaravel.id_post = self.driver.current_url.split('/')[-1]

    def EditPost(self):
        self.user_login('laravel1@yahoo.com', 'lary30')
        self.assertIn('Dashboard', self.driver.page_source)
        time.sleep(2)

        edit_button = self.driver.find_element_by_id('edit-post-' + Testlaravel.id_post + '' )
        edit_button.click()
        time.sleep(2)

        submit_button = self.driver.find_element_by_xpath('//*[@id="submit_post"]')
        submit_button.click()
        time.sleep(2)
        self.assertIn('Post updated', self.driver.page_source)

    def DeletePost(self):
        self.user_login('laravel1@yahoo.com', 'lary30')
        self.assertIn('Dashboard', self.driver.page_source)
        time.sleep(2)
        delete_button= self.driver.find_element_by_id('delete-post-' + Testlaravel.id_post + '')
        delete_button.click()
        self.assertIn('Post deleted', self.driver.page_source)


    def EditUser(self):
        self.user_login('laravel1@yahoo.com', 'lary30')
        self.assertIn('Dashboard', self.driver.page_source)
        time.sleep(2)

        settings_button = self.driver.find_element_by_xpath('//*[@id="sidebar-settings"]/a')
        settings_button.click()
        time.sleep(2)
        self.assertIn('/settings', self.driver.current_url)

        edit_button = self.driver.find_element_by_xpath('//*[@id="edit_account"]')
        edit_button.click()
        time.sleep(2)
        self.assertIn('/edit', self.driver.current_url)

        change_user = self.driver.find_element_by_xpath('/html/body/main/div/div/main/form/select')
        change_user.click()

        select_admin = self.driver.find_element_by_xpath('//*[@id="admin"]')
        select_admin.click()

        submit_button = self.driver.find_element_by_xpath('//*[@id="apply"]')
        submit_button.click()
        self.assertIn('Users', self.driver.page_source)
        self.assertIn('User updated', self.driver.page_source)

    def EditEmail(self):
        self.user_login('laravel1@yahoo.com', 'lary30')
        self.assertIn('Dashboard', self.driver.page_source)
        time.sleep(2)

        settings_button = self.driver.find_element_by_xpath('//*[@id="sidebar-settings"]/a')
        settings_button.click()
        time.sleep(2)
        self.assertIn('/settings', self.driver.current_url)

        edit_button = self.driver.find_element_by_xpath('//*[@id="edit_account"]')
        edit_button.click()
        time.sleep(2)
        self.assertIn('/edit', self.driver.current_url)

        email_box = self.driver.find_element_by_xpath('//*[@id="edit_email"]')
        email_box.clear()
        email_box.send_keys('laravel1@yahoo.com')

        submit_button = self.driver.find_element_by_xpath('//*[@id="apply"]')
        submit_button.click()
        self.assertIn('User updated', self.driver.page_source)

    def EditOtherUser(self):
        self.user_login('laravel1@yahoo.com', 'lary30')
        self.assertIn('Dashboard', self.driver.page_source)
        time.sleep(2)

        users_button = self.driver.find_element_by_xpath('//*[@id="sidebar-users"]/a')
        users_button.click()
        self.assertIn('/admin/users', self.driver.current_url)

        edit_button = self.driver.find_element_by_xpath('//*[@id="edit-user-4"]')
        edit_button.click()
        time.sleep(2)

        submit_button = self.driver.find_element_by_xpath('// *[ @ id = "apply"]')
        submit_button.click()
        self.assertIn('admin/users', self.driver.current_url)


# def main():
#     test_suite = unittest.TestSuite()
#
#     test_suite.addTest(Testlaravel('test_DeleteAcount'))
#     test_suite.addTest(Testlaravel('test_registerUser'))
#
#     runner = unittest.TextTestRunner(verbosity=3)
#     runner.run(test_suite)




def main(test_plan):

    test_suite = unittest.TestSuite()
    fisier = open(test_plan, 'r')
    lines = fisier.read().split('\n')
    for line in lines :
        test_suite.addTest(Testlaravel(line))
    fisier.close()

    runner = unittest.TextTestRunner(verbosity=3)
    runner.run(test_suite)

main(sys.argv[1])






















# +id_postare +''
# numeClasa.id_postare = ...(id din url)...

# Functionalitai
# -LOGIN
# -Register
# -Blog
# _Acces control
#
# Login:
# email+parola
# redirectionare cater dashboard
#
# Register
# -name, email, parolax2
# -redirectionare catre dashboard
# -defaul role:user
#
# Blog:
# -postarile sunt legate de user(doar autorul are dreptedit/delete)
# -titlu+ continut
# edi/delete
#
# Acess control:
# -Roluri:Administrator/User
# -User are dreptul sa posteze pe blog
# ***User isi poate modifica rol in admin
# -Admin are drepul sa posteze pe blog
# -Admin are drepul sa steargaconturi
# -Admin are drepul sa editeze conturi
# parolele nu se sterg

# 1. accesare website
# 2. click button register
# 3. validam ca am ajuns pe pagina buna
# 4. introducem informatiile(nume, email, parola, confirmare parola)
# 5. click buton register***BUTONUL ALBASTRU, DE SUBMIT
# 6. validare inreg succesful
#
#  cazuri test
# 1.
# register +validare
# 2. login + validare
# 3.add new post
# 4. edit post
# 5. delete post
# 6.edit user
# 7.edit other users
# 8.delete other user
# 9. delete user

